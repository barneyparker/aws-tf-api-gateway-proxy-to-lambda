resource "aws_api_gateway_domain_name" "gateway_domain_regional" {
  count = "${var.regional == "true" ? 1 : 0}"

  domain_name              = "${var.custom_domain}"
  regional_certificate_arn = "${var.certificate_arn}"

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "gateway_mapping_regional" {
  count = "${var.regional == "true" ? 1 : 0}"

  api_id      = "${aws_api_gateway_rest_api.api_gateway.id}"
  stage_name  = "${aws_api_gateway_deployment.stage.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.gateway_domain_regional.domain_name}"

  base_path = "${var.mapping}"
}

resource "aws_api_gateway_domain_name" "gateway_domain_edge" {
  count = "${var.regional == "false" ? 1 : 0}"

  domain_name              = "${var.custom_domain}"
  certificate_arn = "${var.certificate_arn}"

  endpoint_configuration {
    types = ["EDGE"]
  }
}

resource "aws_api_gateway_base_path_mapping" "gateway_mapping_edge" {
  count = "${var.regional == "false" ? 1 : 0}"

  api_id      = "${aws_api_gateway_rest_api.api_gateway.id}"
  stage_name  = "${aws_api_gateway_deployment.stage.stage_name}"
  domain_name = "${aws_api_gateway_domain_name.gateway_domain_edge.domain_name}"

  base_path = "${var.mapping}"
}
