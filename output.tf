output "endpoint" {
  value = "${aws_api_gateway_deployment.stage.invoke_url}"
}

output "created_date" {
  value = "${aws_api_gateway_rest_api.api_gateway.created_date}"
}

output "proxy_domain" {
  value = "${var.regional == "true" ? join("", aws_api_gateway_domain_name.gateway_domain_regional.*.regional_domain_name) : join("", aws_api_gateway_domain_name.gateway_domain_edge.*.cloudfront_domain_name)}"
}
