variable "name" {
  type        = "string"
  description = "API Name"
}

variable "description" {
  type        = "string"
  description = "API Description"
}

variable "stage" {
  type        = "string"
  description = "Name for the API Stage"
}

variable "lambda_function_name" {
  type        = "string"
  description = "Lambda Function Name to proxy to"
  default     = "default_value"
}

variable "lambda_invoke_arn" {
  type        = "string"
  description = "ARN for Lambda to proxy to"
}

variable "regional" {
  type        = "string"
  description = "Regional or Edge (Boolean)"
  default     = "false"
}

variable "custom_domain" {
  type        = "string"
  description = "Custom domain name"
  default     = ""
}

variable "certificate_arn" {
  type        = "string"
  description = "Certifiacte ARN for custom domain"
  default     = ""
}

variable "mapping" {
  type        = "string"
  description = "API Stage Domain Mapping"
  default     = ""
}
